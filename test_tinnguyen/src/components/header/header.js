import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import COLORS from '../../utils/colors';

const Header = props => {
  const dateObject = new Date();

  return (
    <View style={styles.container}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={[COLORS.green, COLORS.blue]}
        style={styles.linearGradient}>
        <Text style={styles.textTitle}>
          Lịch khám ngày {dateObject.getDate()}/{dateObject.getMonth()}/
          {dateObject.getFullYear()}
        </Text>
      </LinearGradient>
    </View>
  );
};

export default Header;
