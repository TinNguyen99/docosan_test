import {StyleSheet} from 'react-native';
import {responsiveWidth, responsiveHeight} from '../../utils/responsive';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: responsiveHeight(50),
  },
  linearGradient: {
    flex: 1,
    paddingHorizontal: responsiveWidth(15),
  },
  textTitle: {
    fontSize: 18,
    fontFamily: 'Roboto',
    textAlign: 'center',
    fontWeight: 'bold',
    marginHorizontal: responsiveWidth(10),
    marginVertical: responsiveHeight(10),
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});

export default styles;
