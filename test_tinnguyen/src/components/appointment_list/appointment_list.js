import React from 'react';
import {View, Text, ScrollView} from 'react-native';
import {timeList} from '../../constants/constants';
import AppointmentCard from '../appointment_card/appointment_card';
import styles from './style';
import {calculateAppointmentTime} from '../../utils/responsive';

const AppointmentList = props => {
  return (
    <ScrollView style={styles.scrollStyle}>
      {timeList.map(e => {
        const valueTime = calculateAppointmentTime(props.currentMinutes);
        const isNewDay = e.time === '00' ? true : false;
        return (
          <View key={e.time} style={styles.container}>
            <View style={styles.groupTime}>
              <View
                style={
                  isNewDay
                    ? [styles.positionTime, {top: 0}]
                    : styles.positionTime
                }>
                <Text style={styles.textTime}>{e.time}:00</Text>
              </View>
            </View>
            <View style={styles.groupContent}>
              {e.appoitment_calendar.length > 0 ? (
                e.appoitment_calendar.map(item => {
                  return (
                    <View key={item.appointment_id} style={styles.groupCard}>
                      <AppointmentCard data={item} />
                      {props.currentHours === parseInt(e.time) ? (
                        <View
                          style={[
                            styles.lineCurrentTimeWithCard,
                            {marginTop: valueTime - 20},
                          ]}
                        />
                      ) : null}
                    </View>
                  );
                })
              ) : (
                <View style={styles.groupCard}>
                  {props?.currentHours === parseInt(e.time) ? (
                    <View
                      style={[
                        styles.lineCurrentTimeNotCard,
                        {marginTop: valueTime},
                      ]}
                    />
                  ) : null}
                </View>
              )}
            </View>
          </View>
        );
      })}
    </ScrollView>
  );
};

export default AppointmentList;
