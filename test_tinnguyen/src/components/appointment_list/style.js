import {StyleSheet} from 'react-native';
import {responsiveHeight} from '../../utils/responsive';
import COLORS from '../../utils/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,

    height: 100,
    width: '84%',
    marginStart: 10,
    flexDirection: 'row',
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
  },
  textTitle: {
    fontSize: 18,
    fontFamily: 'Roboto',
    textAlign: 'center',
    fontWeight: 'bold',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  scrollStyle: {
    paddingTop: 10,
    flex: 1,
    marginBottom: responsiveHeight(60),
  },
  groupTime: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderRightWidth: 1,
    width: 50,
    borderColor: COLORS.grey,
    height: '100%',
  },
  positionTime: {
    position: 'absolute',
    top: -10,
  },
  textTime: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    paddingRight: 5,
    backgroundColor: 'white',
  },
  groupContent: {
    paddingStart: 5,
    borderTopWidth: 1,
    borderColor: COLORS.grey,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  groupCard: {
    flex: 1,
    width: '60%',
    alignContent: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  lineCurrentTimeWithCard: {
    borderTopWidth: 2,
    height: responsiveHeight(95),
    width: '100%',
    borderTopColor: COLORS.pink,
    position: 'absolute',
  },
  lineCurrentTimeNotCard: {
    borderTopWidth: 2,
    height: responsiveHeight(100),
    width: '100%',
    borderTopColor: COLORS.pink,
  },
});

export default styles;
