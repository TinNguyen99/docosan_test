import {StyleSheet} from 'react-native';
import {responsiveWidth, responsiveHeight} from '../../utils/responsive';
import COLORS from '../../utils/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: responsiveHeight(30),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    borderColor: COLORS.green,
    borderWidth: 2,
    position: 'absolute',
    bottom: responsiveHeight(15),
  },
  textTitle: {
    fontSize: 18,
    fontFamily: 'Roboto',
    textAlign: 'center',
    fontWeight: 'bold',
    marginHorizontal: responsiveWidth(10),
    marginVertical: responsiveHeight(10),
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  groupItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: responsiveWidth(10),
  },
  title: {
    fontFamily: 'Roboto',
  },
});

export default styles;
