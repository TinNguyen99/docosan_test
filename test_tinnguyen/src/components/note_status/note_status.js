import React from 'react';
import {View, Text} from 'react-native';
import styles from './style';
import COLORS from '../../utils/colors';
import IconStatus from '../icon_status/icon_status';

const NoteStatus = () => {
  return (
    <View style={styles.container}>
      <View style={styles.groupItem}>
        <IconStatus colorStatus={COLORS.grey} />
        <Text style={styles.title}>Passed</Text>
      </View>
      <View style={styles.groupItem}>
        <IconStatus colorStatus="red" />
        <Text style={styles.title}>Pending</Text>
      </View>
      <View style={styles.groupItem}>
        <IconStatus colorStatus={COLORS.green} />
        <Text style={styles.title}>Approved</Text>
      </View>
    </View>
  );
};

export default NoteStatus;
