import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import IconStatus from '../icon_status/icon_status';
import styles from './style';
import COLORS from '../../utils/colors';

const AppointmentCard = props => {
  const colorStatus =
    props.data.status === 'approved'
      ? COLORS.green
      : props.data.status === 'pending'
      ? 'red'
      : COLORS.grey;

  return (
    <TouchableOpacity style={styles.container} onPress={() => {}}>
      <View
        style={[styles.itemColor, {backgroundColor: props.data.color_code}]}
      />
      <View style={styles.groupInfo}>
        {props.data.avatar != null ? (
          <Image
            style={styles.imageStyle}
            source={{
              uri: props.data.avatar,
            }}
          />
        ) : (
          <Image
            style={styles.imageStyle}
            source={require('../../image/avatarAnynomousFemale.jpg')}
          />
        )}

        <Text
          style={styles.requesterStyle}
          ellipsizeMode="tail"
          numberOfLines={1}>
          {props.data.requester}
        </Text>
        <View style={styles.groupStatus}>
          <IconStatus colorStatus={colorStatus} />
          <Text
            style={styles.symptomStyle}
            ellipsizeMode="tail"
            numberOfLines={1}>
            {props.data.symptom}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default AppointmentCard;
