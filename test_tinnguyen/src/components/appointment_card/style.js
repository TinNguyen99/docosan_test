import {StyleSheet} from 'react-native';
import {responsiveWidth, responsiveHeight} from '../../utils/responsive';
import COLORS from '../../utils/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    height: responsiveHeight(90),
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
    borderRadius: 10,
    borderColor: COLORS.grey,
    borderWidth: 1,
    paddingEnd: responsiveWidth(10),
  },
  itemColor: {
    width: responsiveWidth(10),
    height: '100%',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  imageStyle: {
    width: responsiveWidth(20),
    height: responsiveHeight(20),
    borderRadius: 10,
  },
  textSymptom: {
    paddingEnd: responsiveWidth(10),
    fontFamily: 'Roboto',
  },
  groupStatus: {
    flexDirection: 'row',
  },
  groupInfo: {
    flex: 1,
    paddingStart: responsiveWidth(10),
  },
  requesterStyle: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 14,
  },
  symptomStyle: {
    fontFamily: 'Roboto',
    fontSize: 12,
  },
});

export default styles;
