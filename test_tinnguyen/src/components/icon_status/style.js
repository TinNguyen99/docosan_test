import {StyleSheet} from 'react-native';
import {responsiveWidth, responsiveHeight} from '../../utils/responsive';

const styles = StyleSheet.create({
  container: {
    width: responsiveWidth(12),
    height: responsiveHeight(12),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    marginVertical: responsiveHeight(5),
    marginRight: responsiveHeight(5),
  },
  point: {
    width: responsiveWidth(4),
    height: responsiveHeight(4),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    backgroundColor: 'white',
  },
});

export default styles;
