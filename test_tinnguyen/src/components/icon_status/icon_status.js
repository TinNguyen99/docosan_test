import React from 'react';
import {View} from 'react-native';
import styles from './style';

const IconStatus = colorStatus => {
  return (
    <View
      style={[styles.container, {backgroundColor: colorStatus.colorStatus}]}>
      <View style={styles.point} />
    </View>
  );
};

export default IconStatus;
