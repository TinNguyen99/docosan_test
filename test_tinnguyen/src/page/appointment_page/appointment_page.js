import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import styles from './style';
import AppointmentList from '../../components/appointment_list/appointment_list';
import Header from '../../components/header/header';
import NoteStatus from '../../components/note_status/note_status';

const dateObject = new Date();
const AppointmentPage = () => {
  const [hoursNow, sethourNow] = useState(dateObject.getHours());
  const [minutesNow, setminutesNow] = useState(dateObject.getMinutes());
  useEffect(() => {
    const interval = setInterval(() => {
      sethourNow(dateObject.getHours());
      setminutesNow(dateObject.getMinutes());
    }, 60000);
    return () => clearInterval(interval);
  }, [minutesNow]);

  return (
    <View style={styles.container}>
      <Header />
      <AppointmentList currentHours={hoursNow} currentMinutes={minutesNow} />
      <NoteStatus />
    </View>
  );
};

export default AppointmentPage;
