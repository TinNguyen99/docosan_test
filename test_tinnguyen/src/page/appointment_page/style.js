import {StyleSheet} from 'react-native';
import {responsiveWidth, responsiveHeight} from '../../utils/responsive';
import COLORS from '../../utils/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  scrollStyle: {
    paddingTop: responsiveHeight(10),
    marginBottom: responsiveHeight(60),
  },
});

export default styles;
