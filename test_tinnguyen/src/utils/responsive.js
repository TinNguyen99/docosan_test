import {Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

export const responsiveWidth = value => (value / 414) * width;

export const responsiveHeight = value => (value / 736) * height;

export const calculateAppointmentTime = minutes => {
  const value = (190 * minutes) / 59;
  return parseInt(value.toFixed());
};
