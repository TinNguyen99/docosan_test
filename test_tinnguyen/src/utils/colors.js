export default {
  green: '#4AC0A4',
  blue: '#70BDE9',
  dark_blue: '#072D60',
  yellow: '#FFC455',
  pink: '#D62D68',
  orange: '#E8674D',
  grey: '#d3d3d3',
};
