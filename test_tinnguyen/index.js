/**
 * @format
 */

import {AppRegistry} from 'react-native';
import AppointmentPage from './src/page/appointment_page/appointment_page';

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => AppointmentPage);
